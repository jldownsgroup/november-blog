CONTAINER_NAME="uniquepythondev"

docker run \
    -it \
    -v $(pwd):/home \
    $CONTAINER_NAME \
    /bin/bash
