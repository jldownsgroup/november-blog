import logging
from slugify import slugify
import subprocess
from bs4 import BeautifulSoup
import os

logging.basicConfig(level=logging.DEBUG)



def html_from_url(url):
    cmd = f"chromium --no-sandbox --headless --disable-gpu --dump-dom {url}"
    html = subprocess.run(cmd.split(" "), capture_output=True).stdout.decode()
    return html

def ample_id_from_url(url):
    assert "public.amplenote.com" in url
    return url.split("/")[-1]

class AmpleSite:

    # This class variable keeps track of all the pages we have already
    # parsed.
    page_from_id = {}

    def __init__(self, url, base_url, base_path):
        self.ample_url = url
        self.base_url = base_url
        self.base_path = base_path

        # register this site with the global site-keeper-tracker.
        # nasty, I know.
        self.ample_id = ample_id_from_url(url)
        self.page_from_id[self.ample_id] = self
        self.parse()

    def local_url(self, base_url):
        """Returns the slugified url."""
        return os.path.join(base_url, self.slug)

    def parse(self):
        logging.info(f"Parsing {self.ample_url}...")

        # download page from Amplenote
        self.html = html_from_url(self.ample_url)
        soup = BeautifulSoup(self.html, "html.parser")
        
        # parse title and create url slug
        self.title = soup.find_all("div", class_="note-name")[0].get_text()
        self.slug = slugify(self.title)

        # now that we have the slug, calculate the web url and local
        # foldername
        self.folder = os.path.join(self.base_path, self.slug)
        self.local_url = os.path.join(self.base_url, self.slug)

        # strip off amplenote's header and footer.
        user_area_div = soup.find_all("div", class_="ample-editor-editor")[0]

        ##### Deamplenoteify ######
        # find and clean links
        for e in user_area_div.find_all("a", class_="heading-anchor"):
            e.decompose()
        for e in user_area_div.find_all("span", class_="overlay-button"):
            e.decompose()
        for e in user_area_div.find_all("div", class_="buttons-container"):
            e.decompose()
        for e in user_area_div.find_all("i", class_="icon"):
            e.decompose()
        for e in user_area_div.find_all("div", class_="visible-list-item"):
            e.name = "li"
        for e in user_area_div.find_all("div", class_="bullet-list-item"):
            e.name = "ul"
        for e in user_area_div.find_all("h1", class_="heading"):
            e.string = e.get_text()

        div_elements = user_area_div.find_all("div")
        for d in div_elements:
            del d["data-uuid"]

        link_elements = user_area_div.find_all("a")
        for l in link_elements:
            del l["target"]
            del l["data-href"]


        ###########################

        # collect public links so we can change them to relative links
        self.links = [x["href"] for x in link_elements if x.has_attr("href")]
        self.public_links = [x for x in self.links if "public" in x]

        # collect the inner html
        self.note_html = user_area_div.prettify()

        # translate the ample links into local links
        local_link_from_ample = []
        for l in self.public_links:
            ample_id = ample_id_from_url(l)
            # Parse site if it doesn't exist yet
            if not (ample_id in self.page_from_id):
                AmpleSite(l, self.base_url, self.base_path)

            page_object = self.page_from_id[ample_id_from_url(l)]
            local_link_from_ample.append((l, page_object.local_url))

        # replace the ample links with local links
        for ample_link, local_link in local_link_from_ample:
            logging.debug(f"Replacing {ample_link} with {local_link}")
            self.note_html = self.note_html.replace(ample_link, local_link)



        

