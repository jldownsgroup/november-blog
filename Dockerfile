FROM python:3.8-buster

WORKDIR /home

RUN apt update && apt install -y chromium

ADD requirements.txt /home/

RUN pip install -r requirements.txt

