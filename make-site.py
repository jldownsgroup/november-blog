from get_page import AmpleSite
from gen_page import *
import os
import shutil

url = "https://public.amplenote.com/JwHyxggTrFcZf4x6TDW11BZJ"
base_url = "https://jldownsgroup.gitlab.io/november-blog/"
base_path = os.path.join(os.getcwd(), "public")
s = AmpleSite(url, base_url, base_path)

# Warning! This deletes the base path and contents!
try:
    shutil.rmtree(base_path)
except FileNotFoundError:
    pass

# move stylesheet over
os.makedirs("./public")
shutil.copyfile("./blog.css", "./public/blog.css")
shutil.copyfile("./normalize.css", "./public/normalize.css")
shutil.copyfile("./skeleton.css", "./public/skeleton.css")

for page in s.page_from_id.values():
    print(page.title)
    
    # write file
    os.makedirs(page.folder, exist_ok=True)
    with open(os.path.join(page.folder, "index.html"), "w") as f:
        f.write(html_page(page.note_html, page.title, base_url))

