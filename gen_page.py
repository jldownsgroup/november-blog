import os


def html_page(inner_html, title, base_url):
    return header_html(title, base_url) + inner_html + footer_html()

def header_html(title, base_url):
    return f"""
    <html>
    <head>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{os.path.join(base_url, "normalize.css")}">
        <link rel="stylesheet" href="{os.path.join(base_url, "skeleton.css")}">
        <link rel="stylesheet" href="{os.path.join(base_url, "blog.css")}">
    </head>
    <body>
    <div class="container">

    <div class="row header">
        <div class = "one column header-button">
        Home
        </div>
        <div class = "one column header-button">
        Index
        </div>
        <div class = "one column header-button">
        Legal
        </div>
    </div>
    """

def footer_html():
    return f"""
    </div>
    </body>
    </html>
    """


def embedded_html_page(token):

    page = f"""
    <html>

    <head>
    </head>

    <body>

    <div class="amplenote-embed" data-note-token="{token}">
    <iframe frameborder="0" src="https://public.amplenote.com/embed/{token}?hostname=jldownsgroup.gitlab.io/november-blog"></iframe>
    </div>
    <script defer src="https://public.amplenote.com/embed.js"></script>

    </body>

    </html>
    """

    return page

